var scene, camera, renderer, sceneContainer;
var controller;
var gui = null;

$(document).ready(function(){
  var WIDTH = window.innerWidth,
      HEIGHT = window.innerHeight;

  var mouse = new THREE.Vector2(), INTERSECTED;
  var mesh, color;

  var de2ra = function(degree) { return degree*(Math.PI/180);};

  init();
  animate();

  function init() {
    sceneContainer = document.getElementById('sceneContainer');
    scene = new THREE.Scene();

    var panelSize = 5

    raycaster = new THREE.Raycaster();
    renderer = new THREE.WebGLRenderer({antialias:true});
    renderer.setSize(WIDTH, HEIGHT);
    renderer.setClearColor(0x333F47, 1);

    sceneContainer.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 1 , 1000);
    camera.position.set(0, 12, 12);
    camera.lookAt(scene.position);

    scene.add(camera);

    var gui = new dat.GUI();

    controller = new THREE.Object3D();
    controller.objects = [];
    controller.scene = scene;
    controller.gui = gui;
    controller.cube_scale = 1;
    controller.number_of_objects = 1;
    controller.selected_cube = 'sampleCube';
    controller.total_count = 1;

    controller.createNew = function() {

      color = Math.random() * 0xffffff;
      var geometry = new THREE.BoxGeometry( 2, 2, 2 );
      var material = new THREE.MeshLambertMaterial({
        ambient: color,
        color: color,
        transparent: true
      });

      var cube = new THREE.Mesh(geometry, material);

      cube.name = 'cube_' + controller.objects.length;
      cube.position.x = Math.random() * (panelSize * 2) - panelSize;
      cube.position.z = Math.random() * (panelSize * 2) - panelSize;
      cube.rotation.set(0, 0, 0);
      cube.rotation.y = de2ra(-90);
      cube.scale.set(1, 1, 1);
      cube.doubleSided = true;
      cube.castShadow = true;

      cube.material.color.setHex(color);

      var object3d  = new THREE.DirectionalLight('white', 0.55);
      object3d.position.set(6,3,9);
      object3d.name = 'Back light';
      controller.scene.add(object3d);

      object3d = new THREE.DirectionalLight('white', 0.55);
      object3d.position.set(-6, -3, 0);
      object3d.name   = 'Key light';
      controller.scene.add(object3d);

      object3d = new THREE.DirectionalLight('white', 0.55);
      object3d.position.set(9, 9, 6);
      object3d.name = 'Fill light';
      controller.scene.add(object3d);
      controller.scene.add(cube);

      controller.objects.push(cube);
      controller.selected_cube = cube.name;
      controller.total_count = controller.objects.length;
    };

    var spotLight = new THREE.SpotLight( 0xffffff );
    spotLight.position.set( 3, 30, 3 );
    spotLight.castShadow = true;
    spotLight.shadowMapWidth = 2048;
    spotLight.shadowMapHeight = 2048;
    spotLight.shadowCameraNear = 1;
    spotLight.shadowCameraFar = 4000;
    spotLight.shadowCameraFov = 45;
    scene.add( spotLight );

    controls = new THREE.OrbitControls(camera, renderer.domElement);
    window.addEventListener( 'resize', onWindowResize, false );

    gui.add(controller, 'total_count').name('Total Cubes').listen();
    gui.add(controller, 'selected_cube').name('Selected Cube').listen();
    gui.add(controller, 'createNew').name('Add Cube');

    var f1 = { 
      changeColor:function(){
        for (var i = 0; i < controller.objects.length; i++) {
            if (controller.objects[i].name == controller.selected_cube )
            {
              color = Math.random() * 0xffffff;
              controller.objects[i].material.color.setHex(color);
              break;
            }
        }
      }
    };

    var f2 = { 
      resetCamera:function(){ 
        camera.position.set(0, 12, 12);
        camera.lookAt(scene.position);
      }
    };

    var f3 = { 
      defaultSize:function(){
        for (var i = 0; i < controller.objects.length; i++) {
          controller.objects[i].scale.set(1, 1, 1);
        }
      }
    };

    var f4 = { 
      deleteCube:function(){
        for (var i = 0; i < controller.objects.length; i++) {
          if (controller.objects[i].name == controller.selected_cube )
          {
            var selectedObject = controller.scene.getObjectByName(controller.objects[i].name);
            scene.remove( selectedObject );
            animate();
            break;
          }
        }
      }
    };

    gui.add(f1, 'changeColor').name('Change Color');
    gui.add(f2, 'resetCamera').name('Reset Camera');
    gui.add(f3, 'defaultSize').name('Reset Scale');
    gui.add(f4, 'deleteCube').name('Delete Cube');

    gui.add( controller, 'cube_scale', 1, 5 ).onChange( function() {
      s = (controller.cube_scale);
      for (var i = 0; i < controller.objects.length; i++) {
        controller.objects[i].scale.set(s, s, s);
      }
    });

    controller.createNew();

    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
  }

  function onDocumentMouseDown( event ) {    
      event.preventDefault();
      var mouse3D = new THREE.Vector3( ( event.clientX / window.innerWidth ) * 2 - 1,   
          -( event.clientY / window.innerHeight ) * 2 + 1,  
          0.5 );     
      var raycaster =  new THREE.Raycaster();
      raycaster.setFromCamera( mouse3D, camera );
      var intersects = raycaster.intersectObjects( controller.objects );
      if ( intersects.length > 0 ) {

          var selectedObject = intersects[0].object;
          controller.selected_cube = selectedObject.name;

          controller.selected_cube = selectedObject.name;
          intersects[ 0 ].object.material.remove;
      }
  }

  function onDocumentMouseMove(event) {
        event.preventDefault();
        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
  }

  function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
  }

  function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderScene();
  }

  function renderScene(){
    camera.lookAt( scene.position );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( scene.children );
    if ( intersects.length > 0 ) {
      if ( INTERSECTED != intersects[ 0 ].object ) {
        if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
        INTERSECTED = intersects[ 0 ].object;
        INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
        INTERSECTED.material.emissive.setHex( 0xffffff );
      }
    } else {
      if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
      INTERSECTED = null;
    }
    renderer.render(scene, camera);
  }
});